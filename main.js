function loadDoc() {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      document.getElementById("info").innerHTML =
      this.responseText;
    }
  };
  xhttp.open("GET", "https://raw.githubusercontent.com/mlaw-nycda/data/master/data", true);
  xhttp.send();
}

function loadPokemon() {
  var request = new XMLHttpRequest();
  request.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      document.getElementById("info").innerHTML = this.responseText;
      data = JSON.parse(this.responseText);
      pokemon = {
        name: data['forms']['0']['name'],
        type1: data.types['0'].type.name,
        type2: data.types['1'].type.name,
        image: data.sprites.front_default
      }
      img = document.createElement('img');
      img.src = pokemon['image'];
      img.width = '134';
      img.height = '188';
      document.body.appendChild(img);
      // document.getElementById("info").innerHTML = pokemon.name;
    }
  };
  request.open("GET", "https://pokeapi.co/api/v2/pokemon/257", true);
  request.send();
}
